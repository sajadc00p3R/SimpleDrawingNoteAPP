## Description

with **Simple Drawing note** app can create MarkDown Files view .MD files create local backup from them and  save Markdoen files in folders

## How To Use

- in first run application Create a folder **"SimpleDrawerApp"** you can Finnd your files in it
- in in editor you have 3 style 
     - Bold
     - italic
     - Strikethrough
     
   But you can use all of MarkDown Tags in your note in next Version You can add pictures  use emojis ,lable and...
- your backups save in Root folder of External Storage of Device "Simpledrawerbackup.zip" in next Version you can choose Uniqe names for it


## Dependency

All Designs Classes are native and Simple Drawer Note just have one dependency for View MarkDown Files you can find it [here](https://github.com/mukeshsolanki/MarkdownView-Android)

## Tests

application Minsdk is 14 (Android 4.0 Ice Cream Sandwich) and test just in Marshmallow if you have problem with it write it on issues tab in Gitlab or Github 

