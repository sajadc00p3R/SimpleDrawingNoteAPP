package com.example.c00per.simpledrawingnote.Classes;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.widget.Toast;

import com.example.c00per.simpledrawingnote.ObjectModels.DirectoryObject;
import com.example.c00per.simpledrawingnote.PlainTextEditor;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by c00per on 7/27/17.
 */

public class Backup extends AsyncTask<String, Context, Void> {
    private int BUFFER=8172;
    private List<DirectoryObject> fileslist=new ArrayList<>();
    public void searchtree(String path){
        //this function search root folder of application to found .MD files and save the name and paths in a fileslist
        File rootfolder = new File(Environment.getExternalStorageDirectory() + path);
        if(rootfolder.isDirectory()) {
            String[] child = rootfolder.list();
            if(child.length!=0) {
                for (int counter = 0; counter < child.length; counter++) {
                    searchtree(path+"/"+child[counter]);
                }
            }
        }else{
            DirectoryObject entry=new DirectoryObject();
            entry.setName(rootfolder.getName());
            entry.setPath(rootfolder.getPath());
            fileslist.add(entry);

        }
    }
    public void zip() {

        try {
            BufferedInputStream origin = null;
            FileOutputStream dest = new FileOutputStream(new File(Environment.getExternalStorageDirectory(), "SimpledrawerBackup.zip"));

            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));

            byte data[] = new byte[BUFFER];

            for (int i = 0; i < fileslist.size(); i++) {
                FileInputStream fi = new FileInputStream(fileslist.get(i).getPath());
                origin = new BufferedInputStream(fi, BUFFER);
               if(fileslist.get(i).getPath().startsWith("/storage/")) {
                   //in this loop delete base path of root file
                   String entrystructure = fileslist.get(i).getPath().replaceAll("/storage/emulated/0/SimpleDrawerApp", "");
                   ZipEntry entry = new ZipEntry(entrystructure);
                   out.putNextEntry(entry);
                   int count;
                   while ((count = origin.read(data, 0, BUFFER)) != -1) {
                       out.write(data, 0, count);
                   }
                   origin.close();
               }
            }

            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    protected Void doInBackground(String... strings) {
       searchtree(strings[0]);

        return null;

    }

    @Override
    protected void onPostExecute(Void aVoid) {
        zip();

    }
}
