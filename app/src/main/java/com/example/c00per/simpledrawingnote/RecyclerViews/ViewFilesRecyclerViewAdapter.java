package com.example.c00per.simpledrawingnote.RecyclerViews;

import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.c00per.simpledrawingnote.ObjectModels.DirectoryObject;
import com.example.c00per.simpledrawingnote.R;

import java.util.List;

/**
 * Created by c00per on 7/21/17.
 */

public class ViewFilesRecyclerViewAdapter extends RecyclerView.Adapter<ViewFilesRecyclerViewAdapter.FilesViewHolder> {
    private List<DirectoryObject> files;
    View showfolderscontent;

    public ViewFilesRecyclerViewAdapter(List<DirectoryObject> files) {
        this.files = files;
    }

    @Override
    public FilesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        showfolderscontent  = LayoutInflater.from(parent.getContext()).inflate(R.layout.showfolderscontent, parent, false);
        return new FilesViewHolder(showfolderscontent);
    }

    @Override
    public void onBindViewHolder(FilesViewHolder holder, int position) {
        if(files.get(position).getName().endsWith(".txt")||files.get(position).getName().endsWith(".md")){
            holder.formaticon.setImageResource(R.drawable.notesicon);
        }else if(files.get(position).getName().endsWith(".png") ||files.get(position).getName().endsWith(".bmp")||files.get(position).getName().endsWith(".jpg")){
            holder.formaticon.setImageResource(R.drawable.picsicon);
        }else {
            holder.formaticon.setImageResource(R.drawable.foldersicon);

        }
        holder.filename.setText(files.get(position).getName());

    }


    @Override
    public int getItemCount() {
        return files.size();
    }
    public class FilesViewHolder extends RecyclerView.ViewHolder {
        ImageView formaticon;
        TextView filename;
        public FilesViewHolder(View itemView) {
            super(itemView);
            formaticon=(ImageView)showfolderscontent.findViewById(R.id.formaticon);
            filename=(TextView)showfolderscontent.findViewById(R.id.filename);
        }
    }

    }


