package com.example.c00per.simpledrawingnote;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.mukesh.MarkdownView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;


public class ViewContets extends AppCompatActivity {
    String FILE_PATH,FILE_NAME,CONTENT;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_contets);
        MarkdownView markdownView = (MarkdownView) findViewById(R.id.markdown_view);
        Bundle Extras = getIntent().getExtras();
        FILE_PATH = Extras.getString("path");
        FILE_NAME = Extras.getString("filename");

        File mynotefile = new File(FILE_PATH, FILE_NAME);
        StringBuilder Readfilecontent = new StringBuilder();
        try {
            BufferedReader bufferedReadercontent = new BufferedReader(new FileReader(mynotefile));
            String line;
            while ((line = bufferedReadercontent.readLine()) != null) {
                Readfilecontent.append(line);
                Readfilecontent.append('\n');
            }
            bufferedReadercontent.close();

        } catch (IOException e) {
            Log.d("PlainTextEditor", e.toString());
        }
        CONTENT=Readfilecontent.toString();
        markdownView.setMarkDownText(FILE_NAME+"\n"+"----"+"\n"+CONTENT);

    }
}
