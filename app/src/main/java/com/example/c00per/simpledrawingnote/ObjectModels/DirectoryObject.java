package com.example.c00per.simpledrawingnote.ObjectModels;

/**
 * Created by c00per on 7/24/17.
 */

public class DirectoryObject {
    private String name;
    private String path;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
