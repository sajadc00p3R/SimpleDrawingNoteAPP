package com.example.c00per.simpledrawingnote;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.example.c00per.simpledrawingnote.Classes.Backup;
import com.example.c00per.simpledrawingnote.Classes.Restore;
import com.example.c00per.simpledrawingnote.ObjectModels.DirectoryObject;
import com.example.c00per.simpledrawingnote.RecyclerViews.ViewFilesRecyclerViewAdapter;
import com.example.c00per.simpledrawingnote.RecyclerViews.ViewFilesRecyclerViewClickListener;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ShowFoldersContent extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    RecyclerView showcontentrecycler;
    RecyclerView.Adapter showcontentrecycleradaper;
    List<DirectoryObject> Directorymodel = new ArrayList<>();
    LinearLayoutManager showcontentrecyclerlinear;
    String OPEN_DIRECTORY_PATH = "";
    int SELECT_Zip=100;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_folders_content);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        File target=new File(Environment.getExternalStorageDirectory(), "SimpleDrawerApp");
        Log.d("targetfolder",target.getPath());
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CharSequence createnew[] = new CharSequence[]{"New TextNote", "New Folder", "New DrawerNote"};

                AlertDialog.Builder builder = new AlertDialog.Builder(ShowFoldersContent.this);
                builder.setTitle("New Content");
                builder.setItems(createnew, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            AlertDialog.Builder getname = new AlertDialog.Builder(ShowFoldersContent.this);
                            getname.setMessage("Create New Note");
                            getname.setMessage("Please Enter a name");
                            LayoutInflater inflater = ShowFoldersContent.this.getLayoutInflater();
                            //this is what I did to added the layout to the alert dialog
                            View layout = inflater.inflate(R.layout.alertdialoggetname, null);
                            getname.setView(layout);
                            final EditText nameInput = (EditText) layout.findViewById(R.id.alertgetname);
                            getname.setNegativeButton("Cancel", null);
                            getname.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    if (nameInput.length() > 0) {
                                        try {
                                            File newnote = new File(OPEN_DIRECTORY_PATH, nameInput.getText().toString() + ".md");
                                            FileWriter writer = new FileWriter(newnote);
                                            writer.append("");
                                            writer.flush();
                                            writer.close();
                                            ShowBasecontent(OPEN_DIRECTORY_PATH);
                                            Intent toAddNote = new Intent(ShowFoldersContent.this, PlainTextEditor.class);
                                            toAddNote.putExtra("filename", nameInput.getText().toString() + ".md");
                                            toAddNote.putExtra("path", OPEN_DIRECTORY_PATH);
                                            startActivity(toAddNote);
                                        } catch (IOException e) {
                                            Toast.makeText(ShowFoldersContent.this, "Erro in Create file", Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(ShowFoldersContent.this, "Please input Name", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                            getname.show();
                        } else if (which == 1) {
                            AlertDialog.Builder getname = new AlertDialog.Builder(ShowFoldersContent.this);
                            getname.setMessage("Create New Folder");
                            getname.setMessage("Please Enter a name");
                            LayoutInflater inflater = ShowFoldersContent.this.getLayoutInflater();
                            //this is what I did to added the layout to the alert dialog
                            View layout = inflater.inflate(R.layout.alertdialoggetname, null);
                            getname.setView(layout);
                            final EditText nameInput = (EditText) layout.findViewById(R.id.alertgetname);
                            getname.setNegativeButton("Cancel", null);
                            getname.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    if (nameInput.length() > 0) {
                                        try {
                                            File rootfolder = new File(OPEN_DIRECTORY_PATH, nameInput.getText().toString());
                                            if (!rootfolder.exists()) {
                                                rootfolder.mkdirs();
                                                ShowBasecontent(OPEN_DIRECTORY_PATH);
                                            }
                                        } catch (Exception e) {
                                            Toast.makeText(ShowFoldersContent.this, e.toString(), Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(ShowFoldersContent.this, "Please input Name", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                            getname.show();

                        } else if (which == 2) {
                            Toast.makeText(ShowFoldersContent.this, "Coming Soon", Toast.LENGTH_SHORT).show();

                        }
                    }
                });
                builder.show();
            }
        });
        showcontentrecycler = (RecyclerView) findViewById(R.id.showcontentrecycler);
        showcontentrecycleradaper = new ViewFilesRecyclerViewAdapter(Directorymodel);
        showcontentrecyclerlinear = new LinearLayoutManager(getApplicationContext());
        showcontentrecycler.setLayoutManager(showcontentrecyclerlinear);
        showcontentrecycler.setAdapter(showcontentrecycleradaper);
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);
        showcontentrecycler.addItemDecoration(itemDecoration);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            } else {
                ShowBasecontent(Environment.getExternalStorageDirectory() + "/SimpleDrawerApp");
            }

        } else {
            File rootfolder = new File(Environment.getExternalStorageDirectory(), "/SimpleDrawerApp");
            if (!rootfolder.exists()) {
                rootfolder.mkdirs();
            }
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        showcontentrecycler.addOnItemTouchListener(
                new ViewFilesRecyclerViewClickListener(this, showcontentrecycler, new ViewFilesRecyclerViewClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, final int position) {
                        if (!Directorymodel.get(position).getName().contains(".md")
                                && !Directorymodel.get(position).getName().contains(".bmp")
                                && !Directorymodel.get(position).getName().contains(".jpg")
                                && !Directorymodel.get(position).getName().contains(".png")
                                ) {
                            ShowBasecontent(Directorymodel.get(position).getPath());
                        } else {
                            CharSequence fileactions[] = new CharSequence[]{"View", "Edit"};

                            AlertDialog.Builder Noteoption = new AlertDialog.Builder(ShowFoldersContent.this);
                            Noteoption.setItems(fileactions, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Directorymodel.get(position).getName().contains(".md")) {
                                        if (which == 0) {
                                            Intent viewnote = new Intent(ShowFoldersContent.this, ViewContets.class);
                                            viewnote.putExtra("filename", Directorymodel.get(position).getName());
                                            viewnote.putExtra("path", OPEN_DIRECTORY_PATH);
                                            startActivity(viewnote);
                                        } else if (which == 1) {
                                            Intent Editnote = new Intent(ShowFoldersContent.this, PlainTextEditor.class);
                                            Editnote.putExtra("filename", Directorymodel.get(position).getName());
                                            Editnote.putExtra("path", OPEN_DIRECTORY_PATH);
                                            startActivity(Editnote);
                                        }
                                    }
                                }
                            });
                            Noteoption.show();

                        }
                    }

                    @Override
                    public void onLongItemClick(View view, final int position) {
                        AlertDialog.Builder actionalert = new AlertDialog.Builder(ShowFoldersContent.this);
                        actionalert.setTitle("Delete");
                        actionalert.setMessage("Are You sure ?");
                        actionalert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                DeleteContent(Directorymodel.get(position).getPath());
                            }
                        });
                        actionalert.setNegativeButton("No", null);
                        actionalert.show();

                    }
                }));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_backup) {
            new Backup().execute("/SimpleDrawerApp");
            Toast.makeText(ShowFoldersContent.this,"SimpledrawerBackup.zip Created",Toast.LENGTH_LONG).show();
        } else if (id == R.id.nav_restore) {
            Restore restorebackup=new Restore();
            try {
                restorebackup.unzip();
                Toast.makeText(ShowFoldersContent.this,"Data restored from SimpledrawerBackup.zip",Toast.LENGTH_LONG).show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (id == R.id.nav_GitLab) {
            String url = "https://gitlab.com/sajadc00p3R/SimpleDrawingNoteAPP";
            Intent togitlab = new Intent(Intent.ACTION_VIEW);
            togitlab.setData(Uri.parse(url));
            startActivity(togitlab);
        } else if (id == R.id.nav_exit) {
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {

                        case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    String RootFoldername = "SimpleDrawerApp";
                    File rootfolder = new File(Environment.getExternalStorageDirectory(), RootFoldername);
                    if (!rootfolder.exists()) {
                        rootfolder.mkdirs();
                    }
                    ShowBasecontent("");

                } else {
                    AlertDialog memorypermission = new AlertDialog.Builder(ShowFoldersContent.this).create();
                    memorypermission.setTitle("Need Permissions");
                    memorypermission.setMessage("Simple Drawing Note need permission to create files and folders in your device");
                    memorypermission.setButton(AlertDialog.BUTTON_NEUTRAL, "Give Permissions",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent topakagedetails = new Intent();
                                    topakagedetails.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                                    topakagedetails.setData(uri);
                                    startActivity(topakagedetails);
                                }
                            });
                    memorypermission.show();
                }
            }
        }
    }

    public void ShowBasecontent(String path) {
        //when user cllick on a folder in recyclerview this function change dirctory and update content of recyclerview
        File RootFolder = new File(path);
        OPEN_DIRECTORY_PATH = path;
        File[] RootFolderContent = RootFolder.listFiles();
        if(RootFolderContent!=null) {
            Directorymodel.removeAll(Directorymodel);
            for (int count = 0; count < RootFolderContent.length; count++) {
                DirectoryObject tempobject = new DirectoryObject();
                tempobject.setName(RootFolderContent[count].getName());
                tempobject.setPath(RootFolderContent[count].getPath());
                Directorymodel.add(tempobject);
            }
            showcontentrecycleradaper.notifyDataSetChanged();
        }
    }

    public void DeleteContent(String filepath) {

        File dir = new File(filepath);
        if (dir.isDirectory()) {
            String[] children = dir.list();
            if (children.length != 0) {
                for (int i = 0; i < children.length; i++) {
                   DeleteContent(filepath+"/"+children[i]);
                }
                dir.delete();

                ShowBasecontent(OPEN_DIRECTORY_PATH);
            } else {
                dir.delete();
                ShowBasecontent(OPEN_DIRECTORY_PATH);
            }
        } else {
            dir.delete();
            ShowBasecontent(OPEN_DIRECTORY_PATH);
        }
    }


}
/*

 */