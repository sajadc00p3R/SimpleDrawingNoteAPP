package com.example.c00per.simpledrawingnote;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class PlainTextEditor extends AppCompatActivity {
    String FILE_PATH;
    String FILE_NAME;
    EditText Content;
    CheckBox bold,italic,strikethrough;
    Button saveandexit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plain_text_editor);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Bundle Extras = getIntent().getExtras();
        FILE_PATH = Extras.getString("path");
        FILE_NAME = Extras.getString("filename");
        Content = (EditText) findViewById(R.id.edit_text_note);
        saveandexit = (Button) findViewById(R.id.saveandexit);
        bold = (CheckBox) findViewById(R.id.setbold);

        italic=(CheckBox)findViewById(R.id.setitalic);
        strikethrough=(CheckBox)findViewById(R.id.setstrikethrough);
        File mynotefile = new File(FILE_PATH, FILE_NAME);
        saveandexit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveandexit(FILE_NAME,FILE_PATH);
                finish();
            }
        });
        StringBuilder Readfilecontent = new StringBuilder();
        try {
            BufferedReader bufferedReadercontent = new BufferedReader(new FileReader(mynotefile));
            String line;

            while ((line = bufferedReadercontent.readLine()) != null) {
                    Readfilecontent.append(line);
                    Readfilecontent.append('\n');
            }
            bufferedReadercontent.close();
        } catch (IOException e) {
            Log.d("PlainTextEditor", e.toString());
        }
        Content.setText(Readfilecontent);
        bold.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                setstyle(1);
                 bold.setChecked(isChecked);
            }
        });
        italic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                setstyle(2);
                italic.setChecked(isChecked);
            }
        });
        strikethrough.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                setstyle(3);
                strikethrough.setChecked(isChecked);
            }
        });
    }

    public void setstyle(int stylecode) {
        if (stylecode == 1) {
            String content = Content.getText().toString();
            content = content + "**";
            Content.setText(content);
        }else if(stylecode==2){
            String content = Content.getText().toString();
            content = content + "*";
            Content.setText(content);
        }else if(stylecode==3){
            String content = Content.getText().toString();
            content = content +"~~";
            Content.setText(content);
        }
    }
    public void saveandexit(String name,String path){
        File file = new File(path+"/"+name);
        file.delete();
        try {
            FileWriter writer = new FileWriter(file);
            writer.append(Content.getText().toString());
            writer.flush();
            writer.close();
            Toast.makeText(PlainTextEditor.this,"Successful change text",Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            Log.d("PlainTextEditor",e.toString());
        }

    }
}

