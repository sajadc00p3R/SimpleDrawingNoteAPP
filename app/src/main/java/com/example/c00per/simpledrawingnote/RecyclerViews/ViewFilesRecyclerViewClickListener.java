package com.example.c00per.simpledrawingnote.RecyclerViews;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;

/**
 * Created by c00per on 7/22/17.
 */

public class ViewFilesRecyclerViewClickListener implements RecyclerView.OnItemTouchListener {
    private OnItemClickListener filelistener;



    public interface OnItemClickListener {
        public void onItemClick(View view, int position);

        public void onLongItemClick(View view, int position);
    }
    GestureDetector gestureDetector;
    public ViewFilesRecyclerViewClickListener(Context context, final RecyclerView recyclerView, OnItemClickListener listener) {
        filelistener = listener;
        gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
            @Override
            public void onLongPress(MotionEvent e) {
                View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                if (child != null && filelistener != null) {
                    filelistener.onLongItemClick(child, recyclerView.getChildAdapterPosition(child));
                }
            }
        });
    }
    @Override
    public boolean onInterceptTouchEvent(RecyclerView viewfiles, MotionEvent e) {
        View childView = viewfiles.findChildViewUnder(e.getX(), e.getY());
        if (childView != null && filelistener != null && gestureDetector.onTouchEvent(e)) {
            filelistener.onItemClick(childView, viewfiles.getChildAdapterPosition(childView));
            return true;
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }
}
/*











        @Override public void onTouchEvent(RecyclerView view, MotionEvent motionEvent) { }

        @Override
        public void onRequestDisallowInterceptTouchEvent (boolean disallowIntercept){}
 */